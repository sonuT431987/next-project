
import React, {useState, useEffect} from "react";
import classes from './Login.module.css';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import useTranslation from "next-translate/useTranslation";


const Auth = (props) => {
    const { t } = useTranslation("common");
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [userName,setUsername] = useState('');
    const [userIsValid, setUserIsValid] = useState();
    const [password,setPassword] = useState('');
    const [passwordIsValid, setPasswordIsValid] = useState();
    const [formIsValid, setFormIsValid] = useState(false);
     
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isUser, setIsUser] = useState('');
    
    useEffect(() => {        
        const identifier = setTimeout(() => {
          setFormIsValid(
            userName.trim().length > 4 && password.trim().length > 6
          );
        }, 500);
    
        return () => {
          clearTimeout(identifier);
        };
    }, [userName, password]);

    const validateUserHandler = () => {
        setUserIsValid(userName.trim().length > 4);
      };
    const validatePasswordHandler = () => {
        setPasswordIsValid(password.trim().length > 6);
    };

    const submitForm = (event) => {
        event.preventDefault();
        //loginHandler(userName, password);

        loginHandler();
        localStorage.setItem('isUser', isUser);
        handleClose();        
        setUsername(''); setPassword('');
        // {console.log(localStorage.getItem(isUser))}
    }
    const userNameHandler = (event) => {
        setUsername(event.target.value);
        setIsUser(event.target.value);
    }
    const passwordHandler = (event) => {
        setPassword(event.target.value);
    }   

    useEffect(() => {
        const storedUserLoggedInInformation = localStorage.getItem('isLoggedIn');
        // console.log(storedUserLoggedInInformation);
        if (storedUserLoggedInInformation === '1') {
            setIsLoggedIn(true);
        }
    }, []);
    const loginHandler = (user, password) => {
        localStorage.setItem('isLoggedIn', '1');
        setIsLoggedIn(true);
    };
    const logoutHandler = () => {
        localStorage.removeItem('isLoggedIn');
        setIsLoggedIn(false);
        localStorage.removeItem('isUser');
    };

   
    return(
        <>    
                   
            {!isLoggedIn && 
                <li className="login pointer" onClick={handleShow }>{t("common:login")}</li>
            }
            {/* {setIsUser &&    
                <li>{localStorage.getItem('isUser')}</li>
            } */}
            {isLoggedIn &&            
                <li className="logout pointer" onClick={logoutHandler}>
                {t("common:logout")}</li>
            }

            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header>                
                <Modal.Title>Login</Modal.Title>
                <span className="far fa-times close" onClick={handleClose}></span>
                </Modal.Header>
                <Modal.Body>     
                    <form onSubmit={submitForm}  autoComplete='off'>
                        <div className={`${classes.formGroup} ${
                            userIsValid === false ? classes.invalid : ''
                        }`}>
                            <label htmlFor="userNames">User Name</label>
                            <input className="form-control" 
                            id="userNames" autoComplete="off" 
                            type="text"
                            value={userName}
                            onChange={userNameHandler}
                            onBlur={validateUserHandler}
                            />
                             {!userIsValid ? 
                             <p className={classes.invalid}>Please enter at lest 5 digit</p> :""}                           
                        </div>
                        
                        
                        <div className={`${classes.formGroup} ${
                            passwordIsValid === false ? classes.invalid : ''
                        }`}>
                            <label htmlFor="password">Password</label>
                            <input className="form-control" autoComplete='off' 
                            type="password"
                            id="password"
                            value={password}
                            onChange={passwordHandler}
                            onBlur={validatePasswordHandler}
                            />
                        </div>
                        <div className="d-flex justify-content-between">
                            <Button type="submit" variant="primary mr-3" 
                            className={classes.btn} disabled={!formIsValid}>
                                Save
                            </Button>
                            <Button variant="danger" onClick={handleClose} >
                            Close
                            </Button>
                        </div>
                    </form>
            
                </Modal.Body>
                
            </Modal>

            {/* {error && <ErrorModal title={error.title} 
            message={error.message} />}  */}

           
        </>
        
    )
}



export default Auth;