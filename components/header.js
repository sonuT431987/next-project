import { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import useTranslation from "next-translate/useTranslation";

import Auth from "./auth";

function Header(props) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  useEffect(() => {
    const loginInfo = localStorage.getItem("isLoggedIn");
    if (loginInfo === "1") {
      setIsLoggedIn(true);
    }
  }, []);

  let { t } = useTranslation();
  const router = useRouter();

  return (
    <>
      <header>
        <div className="container">
          <div className="headerwrap">
            <div className="logo">
              <Link href="/">
                <a>
                  <Image
                    src="/images/logo.svg"
                    width={200}
                    height={50}
                    alt="Logo"
                  />
                </a>
              </Link>
            </div>
            <nav>
              <ul className="menu-bar">
                <li>
                  <Link href="/">
                    <a>{t("common:home")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/blog">
                    <a>{t("common:blog")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/contact">
                    <a>{t("common:contact_us")}</a>
                  </Link>
                </li>
              </ul>
            </nav>
            <div className="authwrap">
              <ul>
                <Auth />
                <li className="langwrap">
				<span className="lang-items"><Link href={router.asPath} locale="en">{t("common:en")}</Link></span>
            	<span className="lang-items"><Link href={router.asPath} locale="th">{t("common:th")}</Link></span>

                  {/* {router.locales.map((local) => (
                    <span className="lang-items" key={local}>
                      <Link href={router.asPath} locale={local}>
                        <a>{local}</a>
                      </Link>
                    </span>
                  ))} */}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}

// const Header = () => {

// };

export default Header;
