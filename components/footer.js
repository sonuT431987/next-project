import useTranslation from "next-translate/useTranslation";

const Footer = () => {
	const { t } = useTranslation();
	return (
		<>
			<footer className="footer">
			{t('common:copyright')}
			</footer>
		</>
	);
}

export default Footer;
    