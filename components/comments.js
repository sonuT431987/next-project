import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";



const Comments = ({ commentsD }) => {
  return (
    <>
	<div className="commentspost">	
		<div className="name">{commentsD.name || <Skeleton height={50} />}</div>
		<div className="email">{commentsD.email || <Skeleton height={50} />}</div>
		<div className="body">{commentsD.body || <Skeleton height={150} />}</div>
	</div>
    </>    
  )
}

export default Comments;