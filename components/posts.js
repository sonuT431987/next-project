import Image from "next/image";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";


export default function Posts({PostList}) {
  return (
    <div>
        <div className="card-img-holder">
            {<Image
                src={PostList.thumbnailUrl}
                alt="blog Thumbs"
                width="300"
                height="300"
            /> || <Skeleton height={250} />}
        </div>
        <div className="blog-title">
            <Link href={`/blog/${PostList.id}`}><a>{PostList.title || <Skeleton height={50} />}</a></Link>
        </div>
    </div>
  );
}
