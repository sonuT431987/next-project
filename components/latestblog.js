import Image from "next/image";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";


export default function RelatedSlider({relatedP}) {
  return (
    <div className="cards" key={relatedP.id}>
        <div className="card-img-holder">
            {<Image
                src={relatedP.thumbnailUrl}
                alt="blog Thumbs"
                width="300"
                height="300"
            /> || <Skeleton height={250} />}
        </div>
        <div className="blog-title">
            <Link href={`/blog/${relatedP.id}`}><a>{relatedP.title || <Skeleton height={50} />}</a></Link>
        </div>
    </div>
  );
}
