import Banner from "../components/banner";
import Head from "next/head";
import Pagination from "../components/Pagination";
import RelatedSlider from "../components/latestblog";
import Posts from "../components/posts";
import React, { useState } from "react";
import useTranslation from "next-translate/useTranslation";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";



export async function getStaticProps({ locale }) {

	const [relatedBlog] = await Promise.all([
        fetch("https://jsonplaceholder.typicode.com/photos?_page=3&_limit=50"),
    ]);
    const [BlogSlide] = await Promise.all([
        relatedBlog.json(),
    ])
    
	return {
	  props: {
		BlogSlide
	  }
	}
}


const sliderSettings = {
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: false,
	responsive: [
		{
		breakpoint: 1024,
		settings: {
			slidesToShow: 5,
			slidesToScroll: 3,
		}
		},
		{
		breakpoint: 600,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 2,
			initialSlide: 2
		}
		},
		{
		breakpoint: 480,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		}
		}
	]
};

function HomePage ({ BlogSlide }) {
	
	const [currentPage, setCurrentPage] = useState(1)
	const [postsPerPage] = useState(4)

	const indexOfLastPost = currentPage * postsPerPage;
	// console.log(indexOfLastPost);
	const indexOfFirstPost = indexOfLastPost - postsPerPage;
	// console.log(indexOfFirstPost);

	const PostList = BlogSlide.slice(indexOfFirstPost, indexOfLastPost);
	// console.log("Slice Range", PostList);

	const howManyPages = Math.ceil(BlogSlide.length/postsPerPage);
	// console.log(howManyPages);

	const { t } = useTranslation('common')
	return (
		<>
			{
				<Head>
					<title>Home Page</title>
				</Head>
			}
			<Banner />
			<div className="container">
				<div className="sliders">
					<h2 className="subtitle">{t('common:latestblog')}</h2>
					<Slider {...sliderSettings}>
						{BlogSlide.map((relatedP) => {
							return (
								<div key={relatedP.id}>
									<RelatedSlider relatedP = { relatedP } />     
								</div>
							);
						})}
					</Slider>
				</div>
				<h2 className="subtitle">{t('common:blog')}</h2>
				<div className="postwrap">
					{BlogSlide.slice(indexOfFirstPost, indexOfLastPost).map(PostList => {
						return (
							 <div className="cardwrap"key={PostList.id}>
								<Posts PostList = { PostList } /> 
							</div>
						);
					})}
				</div>
				<Pagination pages={howManyPages} setCurrentPage={setCurrentPage}/>
			</div>
		</>
	)
}

export default HomePage;
