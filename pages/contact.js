import Head from "next/head";
import React, { useState } from "react";
import { useRouter } from "next/router";
import useTranslation from "next-translate/useTranslation";


const Contact = () => {
  const { t } = useTranslation("contact");
  const [submitterName, setSubmitterName] = useState("");
  const router = useRouter();
  const confirmationScreenVisible =
    router.query?.success && router.query.success === "true";
  const formVisible = !confirmationScreenVisible;

  const ConfirmationMessage = (
    <React.Fragment>
      <p>
        Thank you for submitting this form. Someone should get back to you
        within 24-48 hours.
      </p>

      <button
        onClick={() => router.replace("/contact", undefined, { shallow: true })}
      >
        {" "}
        Submit Another Response{" "}
      </button>
    </React.Fragment>
  );

  const ContactForm = (
    <form
      className="container"
      method="POST"
      name="contact-form"
      action="contact/?success=true"
      data-netlify="true"
      data-netlify-honeypot="bot-field"
    >
      <input
        type="hidden"
        name="subject"
        value={`You've got mail from ${submitterName}`}
      />
      <input type="hidden" name="form-name" value="contact-form" />
      <p hidden>
        <label>
          Don’t fill this out: <input name="bot-field" />
        </label>
      </p>
      <div className="form-group">
        <label htmlFor="name">
          {t("contact:name")} <span className="star">*</span>
        </label>
        <input className="form-control"
          id="name"
          name="name"
          required
          onChange={(e) => setSubmitterName(e.target.value)}
          type="text"
        />
      </div>
      <div className="form-group">
        <label htmlFor="company">
		{t("contact:company")} <span className="star">*</span>
        </label>
        <input className="form-control" id="company" name="company" required type="text" />
      </div>
      <div className="form-group">
        <label htmlFor="email">
		{t("contact:email_address")} <span className="star">*</span>
        </label>
        <input className="form-control" id="email" type="email" name="email" required />
      </div>
      <div className="form-group">
        <label htmlFor="message">
		{t("contact:email_address")} <span className="star">*</span>
        </label>
        <textarea className="form-control" id="message" name="message" required />
      </div>
      <button type="submit" className="btn btn-primary">
	  	{t("contact:submit")}
      </button>
    </form>
  );

  return (
    <div className="contactwrap contactform">
      <div className="containers">
        <Head>
          <title></title>
        </Head>
        <h1>{t('contact:contact_us')}</h1>
        {formVisible ? ContactForm : ConfirmationMessage}
      </div>
    </div>
  );
};

export default Contact;
