import Comments from "../../components/comments";
import Image from "next/image";
import { useState } from "react";
import { useRouter } from 'next/router'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import RelatedSlider from "../../components/latestblog";
import Pagination from "../../components/Pagination";
import useTranslation from "next-translate/useTranslation";

// export const getStaticPaths = async ({ locales }) => {
//     const res = await fetch('https://jsonplaceholder.typicode.com/photos')
//     const data = await res.json()
//     const paths = data.map((elm) => {
//         return {
//             paths: [
//               // if no `locale` is provided only the defaultLocale will be generated
//               { params: { pageno: 'post-1' }, locale: 'en' },
//               { params: { pageno: 'post-1' }, locale: 'th' },
//             ],
//             fallback: true,
//           }
//     })
//     return {
//         paths,
//     }

//   }


// export const getStaticPaths = async ({ locales }) => {
//     const res = await fetch('https://jsonplaceholder.typicode.com/photos')
// 	const data = await res.json()

//     const paths = data.map((elm) => {
//         return {
//             params: {
//                 pageno: elm.id.toString(),
//             },
//         };
//     });

//     return {
//         paths,
//         fallback: false,
//     }
// }

export async function getServerSideProps(ctx) {
    const id = ctx.params.pageno;
    const [blogTitle, relatedBlog, CommentsData] = await Promise.all([
        fetch(`https://jsonplaceholder.typicode.com/photos/${id}`),
        fetch("https://jsonplaceholder.typicode.com/photos?_page=3&_limit=50"),
        fetch("https://jsonplaceholder.typicode.com/comments?_page=1&_limit=10")
    ]);
    const [bDetails, BlogSlide, CommentsDatas] = await Promise.all([
        blogTitle.json(),
        relatedBlog.json(),
        CommentsData.json(),
    ])

    
	return {
	  props: {
		bDetails, BlogSlide, CommentsDatas,
        // ...await serverSideTranslations(locale, ['common', 'footer']),
	  }
	}
}




const sliderSettings = {
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: false,
  };

const BlogData = ({ bDetails, BlogSlide, CommentsDatas }) => {
    const router = useRouter();
    const {pageno} = router.query;
    const { t } = useTranslation();
    // console.log("PageNo", pageno);
    // console.log("Router", router);
    const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage] = useState(5);

	const indexOfLastPost = currentPage * postsPerPage;
	// console.log(indexOfLastPost);
	const indexOfFirstPost = indexOfLastPost - postsPerPage;
	// console.log(indexOfFirstPost);

	const commentsD = CommentsDatas.slice(indexOfFirstPost, indexOfLastPost);
	// console.log("Slice Range", PostList);

	const howManyPages = Math.ceil(CommentsDatas.length/postsPerPage);
	// console.log(howManyPages);
  return (
    <div>
        <div className="container">
			<h1 className="main-title">{t('common:blog_details')}</h1>
            <div className="blogbanner">
                <Image src={bDetails.thumbnailUrl} alt="blog Thumbs" width="1500" height="500"/>
            </div>
            <div className="bannerdesc">
                <div className="bannerid font-weight-bold">{bDetails.id}</div>
                <div className="bannertitle">{bDetails.title}</div>
                <div className="bannerdesc">{bDetails.body}</div>
            </div>
            <div className="commentwrap">
                <h2>{t('common:comments')}</h2>
                {CommentsDatas.slice(indexOfFirstPost, indexOfLastPost).map((commentsD) => {
                    return (
                        <div key={commentsD.id}>
                            <Comments commentsD = { commentsD } />
                        </div>
                    );
                })}   
                <Pagination pages={howManyPages} setCurrentPage={setCurrentPage}/>
            </div>
            <div className="sliders">
                <h2>{t('common:latest_blog')}</h2>
                <Slider {...sliderSettings}>
                    {BlogSlide.map((relatedP) => {
                        return (
                            <div key={relatedP.id}>
                                <RelatedSlider relatedP = { relatedP } />
                            </div>     
                        );
                    })}
                </Slider>
            </div>
		</div>
    </div>
  );
}

export default BlogData;
