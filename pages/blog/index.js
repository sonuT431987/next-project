import Image from "next/image";
import Head from "next/head";
import Link from "next/link";

import useTranslation from "next-translate/useTranslation";


export async function  getStaticProps() {
	const res = await fetch("https://jsonplaceholder.typicode.com/photos?_page=3&_limit=12");
	const posts = await res.json();

	
	return {
		props: {
			data: posts,
		},
	};
}




// https://github.com/scalablescripts/next-search/blob/main/components/Products.js

const Blog = ({ data }) => {
	const { t } = useTranslation("blog");
	return (
    <>
      {
        <Head>
          <title>Blog Page</title>
        </Head>
      }
	  <div className="container">
        <h2 className="headwrap">{t("blog:blog")} 
		<select className="form-select">
			<option>Select</option>
			<option value="title">ID</option>
			<option value="desc">Title</option>
		</select>

		</h2>
        <div className="postwrap">
          {data.map(elm => {
            return (
              <div className="cardwrap" key={elm.id}>
				  
                <div className="card-img-holder">
                    <Image
                      src={elm.thumbnailUrl}
                      alt="blog Thumbs"
                      width="300"
                      height="300"
                    />
                </div>
				<div className="blog-title">
					<Link href={`/blog/${elm.id}`}><a>{elm.title}</a></Link>
				</div>
              </div>
            );
          })}
        </div>

      </div>
    </>
  );
};


export default Blog;
