import { useRouter } from "next/router";


const Errorpage = () => {
	const router = useRouter();
	const handleRuter = () => {
		router.push("/")
	}
  return (
    <>
     <div className="notfound">
         <div className="head-404">
            <h1>404</h1>
            <h2>We are sorry, Page not Founs</h2>
         </div>
        <p>this page looking for might hav been removed</p>
        <div className="backlinks"><a onClick={handleRuter}> Back to Home Page</a></div>
    </div> 
    </>
  );
}

export default Errorpage;
