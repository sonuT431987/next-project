const nextTranslate = require("next-translate");




module.exports = {
	...nextTranslate(),
	reactStrictMode: true,
	images: {
		domains: ['via.placeholder.com', 'images.unsplash.com'],
	},
};

